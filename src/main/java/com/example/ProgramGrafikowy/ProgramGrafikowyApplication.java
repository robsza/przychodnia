package com.example.ProgramGrafikowy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramGrafikowyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgramGrafikowyApplication.class, args);
	}
}
