/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author robert
 */
@SpringComponent
public class GabinetList {
    @Autowired
    GabinetRepository repo;

    private List<Gabinet> GabinetList;
    
    
    public List<Gabinet> update()
    {
        return repo.findAll();
    }
    public void add(Gabinet gabinet)
    {
        repo.save(gabinet);
    }
    
    @Transactional
    public void delate(String nazwa){
        repo.deleteByNazwa(nazwa);
    }
    public boolean duplicateNazwa(String nazwa){
    if (repo.findByNazwa(nazwa)!=null)
            return true;
        else
            return false;
    }    
}