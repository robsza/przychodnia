/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Admin
 */
@Entity 
public class Gabinet {
    private int id;
    private String nazwa;
    private String rodzaj;
    private String pietro;
    private Set<Dyzur> dyzury;

    public Gabinet() {
    }

    public Gabinet(String nazwa, String rodzaj, String pietro) {
        this.nazwa = nazwa;
        this.rodzaj = rodzaj;
        this.pietro = pietro;
    }

    
    

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getPietro() {
        return pietro;
    }

    public void setPietro(String pietro) {
        this.pietro = pietro;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }
    
        @OneToMany(mappedBy = "gabinetdyzuru", cascade = CascadeType.ALL)
        public Set<Dyzur> getDyzury() {
            return dyzury;
        }

        public void setDyzury(Set<Dyzur> dyzury) {
            this.dyzury = dyzury;
        }

    @Override
    public String toString() {
        return nazwa;
    }
    
        
}
