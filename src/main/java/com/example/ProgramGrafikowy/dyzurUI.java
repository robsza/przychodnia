package com.example.ProgramGrafikowy;

import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
//import com.vaadin.navigator.Navigator;
//import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinRequest;
//import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
//import com.vaadin.ui.Notification;
//import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
//import java.util.ArrayList;
import java.util.Arrays;
//import java.util.List;
//import java.util.Locale;
//import java.util.jar.Attributes;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author robert
 */
@SpringUI(path = "/dyzury")
@Theme("valo")
public class dyzurUI extends UI {

    private VerticalLayout layout;
    private HorizontalLayout buttonLayout;
    private HorizontalLayout textLayout;
    private HorizontalLayout dyzurLayout;
    private HorizontalLayout buttondyzurLayout;
    private HorizontalLayout menuLayout;
    Button add;
    Button delate;
    Button clean;
    Button adddyzur;
    Button delatedyzur;
    Button switchgabinet;
    

    TextField iddyzuru;
    TextField idosoba;
    TextField idmiesiac;
    TextField idgabinet;
    TextField dyzurfield;
    TextField godzinydyzur;
    TextField dzien;
    ComboBox miesiac;
    Label listadyzurow;
    //NativeSelect ocena;

    Grid<Dyzur> grid;
    @Autowired
    DyzurList dyzurList;
    
    @Autowired
    Service service;

    @Autowired
    MiesiacRepository miesiacrepository;
    private Button switchdyzur;
    private Button switchmain;
    private Button switchlistadyzurow;

    @Override
    protected void init(VaadinRequest request) {
        // Górna część
        setupLayout();
        setupMenuLayout();
        addSubjectComboBox();
        addTable();
        //cleanButton();
        // Dolna część
        addTextFieldDyzur();
        setupDyzurButtonLayout();
        addDyzurButton();
        deleteButton();
        addLabel();
        // testy
        switchuiButton();
    }

    private void setupLayout() {
        layout = new VerticalLayout();
        layout.setSpacing(true);
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(layout);
    }

    
    private void setupMenuLayout(){
        menuLayout = new HorizontalLayout();
        menuLayout.setSpacing(true);
        menuLayout.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        layout.addComponent(menuLayout);
    }

    private void setupDyzurButtonLayout() {
        buttondyzurLayout = new HorizontalLayout();
        buttondyzurLayout.setSpacing(true);
        buttondyzurLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        layout.addComponent(buttondyzurLayout);
    }

    private void configGrid() {
        grid.asSingleSelect().addValueChangeListener(e -> {
            if (e.getValue() == null) {
                clearFields();
            } else {
                Dyzur dyzur = e.getValue();
                iddyzuru.setValue(Integer.toString(dyzur.getId()));
                dyzurfield.setValue(dyzur.getName());
                dzien.setValue(Integer.toString(dyzur.getDzien()));
                godzinydyzur.setValue(Integer.toString(dyzur.getGodziny()));
                idmiesiac.setValue(dyzur.getMiesiacpracy().toString());
                idgabinet.setValue(dyzur.getGabinetdyzuru().toString());
                Osoba d;
                d = dyzur.getOsobadyzurujaca();
                idosoba.setValue(Integer.toString(d.getId()));
                //specjalizacja.setValue(Integer.toStringdyzur.getSpecjalizacja());

            }
        });
        grid.addItemClickListener(e -> {
 
        });

    }


    private void addDyzurButton() {
        adddyzur = new Button("Dodaj nowy Dyzur");
        adddyzur.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        buttondyzurLayout.addComponent(adddyzur);
        adddyzur.addClickListener(e -> {
            service.addDyzur(idosoba.getValue(), idmiesiac.getValue(), idgabinet.getValue(), dyzurfield.getValue(), godzinydyzur.getValue(),dzien.getValue());
            Notification.show("Dodaje dyzur " + dyzurfield.getValue() + " o ilości godzin: " + godzinydyzur.getValue() + " dnia: " + dzien.getValue());
            grid.setItems(dyzurList.update());
        });
    }
    
    private void deleteButton() {
        delate = new Button("Kasuj");
        delate.addStyleName(ValoTheme.BUTTON_DANGER);
        buttondyzurLayout.addComponent(delate);
        delate.addClickListener(e -> {
                service.deleteDyzur(iddyzuru.getValue());
                grid.setItems(dyzurList.update());   
        });
        
    }
    private void switchuiButton(){
        switchgabinet = new Button("Strona Gabinet");
        switchgabinet.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchgabinet);
        switchgabinet.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/gabinet");
            Notification.show("Zmieniam lokalizacje" );
        });
        
        switchdyzur = new Button("Strona Dyzur");
        switchdyzur.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchdyzur);
        switchdyzur.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/dyzur");
            Notification.show("Zmieniam lokalizacje" );
        });
        
        switchmain = new Button("Strona Glowna");
        switchmain.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchmain);
        switchmain.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/main");
            Notification.show("Zmieniam lokalizacje" );
        });
        
        switchlistadyzurow = new Button("Lista Dyzurów");
        switchlistadyzurow.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchlistadyzurow);
        switchlistadyzurow.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/dyzury");
            Notification.show("Zmieniam lokalizacje" );
        });
    }
    /*
    private void cleanButton() {
        clean = new Button("Zamien");
        clean.addStyleName(ValoTheme.BUTTON_PRIMARY);
        buttonLayout.addComponent(clean);
        clean.addClickListener(e -> {
            clearFields();
            delate.setEnabled(false);
        });

    }
    */
    
    private void addTable() {
        grid = new Grid();
        grid.setSizeFull();
        grid.addColumn(Dyzur::getOsobadyzurujaca).setCaption("Osoba");
        grid.addColumn(Dyzur::getMiesiacpracy).setCaption("Miesiac");
        //grid.addColumn(Dyzur::getId).setCaption("Id dyzuru");
        grid.addColumn(Dyzur::getDzien).setCaption("Dzien");
        grid.addColumn(Dyzur::getGodziny).setCaption("Godziny");
        grid.addColumn(Dyzur::getName).setCaption("Nazwa");
        grid.addColumn(Dyzur::getGabinetdyzuru).setCaption("Gabinet");
        //grid.addColumn(Osoba::getOcena).setCaption("Ocena");
        /*
        NativeSelect scores = new NativeSelect();
        scores.setItems("2","3","3.5","4","4.5","5");
        scores.setWidth("30");
        grid.setFrozenColumnCount(0);
        grid.addColumn(Osoba::getOcena).setCaption("Ocena").setEditorComponent(scores, Osoba::setOcena);
        grid.getEditor().setEnabled(true);
        grid.getEditor().addCancelListener((event) -> {
            Notification.show("Anulowano!",Notification.Type.WARNING_MESSAGE);
        });
        grid.getEditor().addSaveListener((event) -> {
            Osoba o = event.getBean();
            personList.add(o);
            Notification.show("Zapisano",Notification.Type.HUMANIZED_MESSAGE);
            grid.setItems(personList.update());
         });
         */
        grid.setItems(dyzurList.update());
        layout.addComponent(grid);
        configGrid();
    }


    private void addTextFieldDyzur() {
        dyzurLayout = new HorizontalLayout();
        iddyzuru = new TextField("Id dyzuru");
        idosoba = new TextField("Id osoby");
        idmiesiac = new TextField("Nazwa miesiaca");
        idgabinet = new TextField("Nazwa gabinetu");
        dyzurfield = new TextField("Nazwa dyzuru");
        godzinydyzur = new TextField("Godziny");
        dzien = new TextField("Dzien");

        dyzurLayout.addComponents(idosoba, idmiesiac, idgabinet, dyzurfield, godzinydyzur,dzien,iddyzuru);
        layout.addComponent(dyzurLayout);
    }

    private void clearFields(TextField... fields) {
        Arrays.asList(fields).stream().forEach((field) -> {
            field.clear();
            field.setErrorHandler(null);
            field.setComponentError(null);
        });
    }

    private void addSubjectComboBox() {
        miesiac = new ComboBox("Wybierz miesiac:");
        miesiac.clear();
        miesiac.setItems(miesiacrepository.findAll()); //PAMIĘTAJ O ZMIANIE toString na name (inaczej pokazuje Obiekt)
        miesiac.addValueChangeListener((event) -> {
            if (event.getValue().toString().equals("----Wybierz miesiac -----")); else {
                //update((Miesiac)event.getValue());
                Notification.show("Wybrano miesiac: " + event.getValue(), Notification.Type.TRAY_NOTIFICATION);
            };
        });

        miesiac.setTextInputAllowed(false);
        miesiac.setEmptySelectionCaption("----Wybierz miesiac -----");
        miesiac.setEmptySelectionAllowed(false);

        layout.addComponent(miesiac);

    }
    
    private void addLabel(){
        listadyzurow = new Label("Lista dyżurów:"
                + ""
                + "");
        layout.addComponent(listadyzurow);
}
}
