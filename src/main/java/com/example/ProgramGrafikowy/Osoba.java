/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author robert
 */
@Entity
public class Osoba {

    private int id;

    private String firstName = "";

    private String lastName = "";

    private String password = "";

    private String login = "";

    private String specjalizacja = "";

    private String email = "";

    private Set<Dyzur> dyzury;

    public Osoba() {
    }

    public Osoba(String lastName) {
        this.lastName = lastName;
    }

    public Osoba(String firstName, String lastName, String email, String specjalizacja) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.specjalizacja = specjalizacja;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @OneToMany(mappedBy = "osobadyzurujaca", cascade = CascadeType.ALL)
    public Set<Dyzur> getDyzury() {
        return dyzury;
    }

    public void setDyzury(Set<Dyzur> dyzury) {
        this.dyzury = dyzury;
    }

    public String getSpecjalizacja() {
        return specjalizacja;
    }

    public void setSpecjalizacja(String specjalizacja) {
        this.specjalizacja = specjalizacja;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName ;
    }
    
}
