/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author robert
 */
public interface DyzurRepository extends JpaRepository<Dyzur, Long> {
    public Dyzur findById (int id);
    public void deleteById(int id);
    public List<Dyzur> findByMiesiacpracy(String miesiacpracy);
}
