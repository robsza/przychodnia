/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import com.vaadin.spring.annotation.SpringComponent;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Admin
 */
@SpringComponent
public class Service {
    
    @Autowired
    DyzurRepository drepo;
    
    @Autowired
    OsobaRepository orepo;
    
    @Autowired
    GabinetRepository grepo;
    
    @Autowired
    MiesiacRepository mrepo;
    
    public void addDyzur(String idosoby, String idmiesiac, String idgabinet, String name, String godziny, String dzien)
    {
     Osoba osoba;
     Miesiac miesiac;
     Gabinet gabinet;
     int godzina = Integer.parseInt(godziny);
     int idosoba = Integer.parseInt(idosoby);
     int dzientmp = Integer.parseInt(dzien);
     osoba = orepo.findById(idosoba);
     miesiac = mrepo.findByName(idmiesiac);
     gabinet = grepo.findByNazwa(idgabinet);
     Dyzur tmp = new Dyzur(name,godzina,dzientmp,osoba,miesiac,gabinet);   
     drepo.save(tmp);
    }
    
    @Transactional
    public void deleteDyzur(String id){
        int iddyzuru = Integer.parseInt(id);
                if (drepo.findById(iddyzuru)!=null){
                Dyzur o = drepo.findById(iddyzuru);
                     Osoba osoba = null;
                     Miesiac miesiac = null;
                     Gabinet gabinet = null;
                
                
                o.setMiesiacpracy(miesiac);
                o.setGabinetdyzuru(gabinet);
                o.setOsobadyzurujaca(osoba);
                
		drepo.deleteById(iddyzuru);
                }
    }
    
    
}
