/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;


import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
//import com.vaadin.navigator.Navigator;
//import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinRequest;
//import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
//import com.vaadin.ui.Notification;
//import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
//import java.util.ArrayList;
import java.util.Arrays;
//import java.util.List;
//import java.util.Locale;
//import java.util.jar.Attributes;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author robert
 */

@SpringUI(path = "/dyzur")
@Theme("valo")
public class grafikUI extends UI{
    private VerticalLayout layout;
    private HorizontalLayout buttonLayout;
    private HorizontalLayout textLayout;
    private HorizontalLayout menuLayout;
    Button add;
    Button delate;
    Button clean;
    
    Button switchgabinet;
    Button switchdyzur;
    Button switchmain;
    
    TextField firstName;
    TextField lastName;
    TextField email;
    TextField specjalizacja;
    ComboBox miesiac;
    //NativeSelect ocena;
    
    Grid<Osoba> grid;
    @Autowired
    PersonList personList;
    //PersonRepository repo;
    private Button switchlistadyzurow;
 

    
    @Override
    protected void init(VaadinRequest request) {
        
        setupLayout();
        setupMenuLayout();
        addSubjectComboBox();
        addTable();
        addTextField();
        setupButtonLayout();
        addButton();
        deleteButton();
        cleanButton();
        switchuiButton();
        

    }
    private void setupLayout(){
        layout = new VerticalLayout();
        layout.setSpacing(true);
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(layout);
    }
    
    private void setupButtonLayout(){
        buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        layout.addComponent(buttonLayout);
    }
    
    private void setupMenuLayout(){
        menuLayout = new HorizontalLayout();
        menuLayout.setSpacing(true);
        menuLayout.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        layout.addComponent(menuLayout);
    }
    
    private void configGrid(){
        grid.asSingleSelect().addValueChangeListener(e->{
            if(e.getValue()==null)
            {
                clearFields();
            }
            else{
            Osoba person = e.getValue();
            firstName.setValue(person.getFirstName());
            lastName.setValue(person.getLastName());
            email.setValue(person.getEmail());
            specjalizacja.setValue(person.getSpecjalizacja());

 
            
            }
        });  
            grid.addItemClickListener(e->{
            delate.setEnabled(true);
            });
        
    }
    
    private void addButton(){
           add = new Button("Dodaj");
           add.addStyleName(ValoTheme.BUTTON_FRIENDLY);
           buttonLayout.addComponent(add);
           add.addClickListener(e->{
               if (personList.duplicateEmail(email.getValue())){
                   email.setComponentError(new UserError("Istnieje już podany emial!!"));
               }
               else{
                    personList.add(new Osoba(firstName.getValue(),lastName.getValue(),email.getValue(),specjalizacja.getValue()));
                    grid.setItems(personList.update());
                    clearFields(firstName,lastName,email,specjalizacja);    
               }
             
           });

    }
        private void deleteButton(){
           delate = new Button("Kasuj");
           delate.addStyleName(ValoTheme.BUTTON_DANGER);
           buttonLayout.addComponent(delate);
           delate.addClickListener(e->{
               if (personList.duplicateEmail(email.getValue())){
               personList.delate(new Osoba(firstName.getValue(),lastName.getValue(),email.getValue(),specjalizacja.getValue()));
               grid.setItems(personList.update());
               }
               else{
                   email.setComponentError(new UserError("Nie ma w bazie podanej osoby!!"));
               }
           });
            delate.setEnabled(false);
            delate.setDisableOnClick(true);
    }
        
        private void switchuiButton(){
        switchgabinet = new Button("Strona Gabinet");
        switchgabinet.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchgabinet);
        switchgabinet.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/gabinet");
            Notification.show("Zmieniam lokalizacje" );
        });
        
        switchdyzur = new Button("Strona Dyzur");
        switchdyzur.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchdyzur);
        switchdyzur.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/dyzur");
            Notification.show("Zmieniam lokalizacje" );
        });
        
        switchmain = new Button("Strona Glowna");
        switchmain.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchmain);
        switchmain.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/main");
            Notification.show("Zmieniam lokalizacje" );
        });
        
        switchlistadyzurow = new Button("Lista Dyzurów");
        switchlistadyzurow.addStyleName(ValoTheme.BUTTON_DANGER);
        menuLayout.addComponent(switchlistadyzurow);
        switchlistadyzurow.addClickListener(e -> {
            String url = Page.getCurrent().getLocation().toString();
            getUI().getPage().setLocation("http://localhost:8080/dyzury");
            Notification.show("Zmieniam lokalizacje" );
        });
    }
        
        
        private void cleanButton(){
            clean = new Button ("Zamien");
            clean.addStyleName(ValoTheme.BUTTON_PRIMARY);
            buttonLayout.addComponent(clean);
            clean.addClickListener(e->{
                clearFields(firstName,lastName,email,specjalizacja);
                delate.setEnabled(false);
        });
          
        }
    
    
    private void addTable(){
        grid = new Grid();
        grid.setSizeFull();
        //grid.addColumn(Osoba::getId).setCaption("Id");
        grid.addColumn(Osoba::getFirstName).setCaption("Imie");
        grid.addColumn(Osoba::getLastName).setCaption("Nazwisko");
        grid.addColumn(Osoba::getEmail).setCaption("Email");
        grid.addColumn(Osoba::getSpecjalizacja).setCaption("Specjalizacja");
        for (int i = 0; i<31 ; i++){
            String tmp= "";
            tmp = Integer.toString(i+1);
            grid.addColumn(Osoba::getLogin).setCaption(tmp);
            //grid.addColumn("").setCaption(tmp);
        }
        //grid.addColumn(Osoba::getOcena).setCaption("Ocena");
        /*
        NativeSelect scores = new NativeSelect();
        scores.setItems("2","3","3.5","4","4.5","5");
        scores.setWidth("30");
        grid.setFrozenColumnCount(0);
        grid.addColumn(Osoba::getOcena).setCaption("Ocena").setEditorComponent(scores, Osoba::setOcena);
        grid.getEditor().setEnabled(true);
        grid.getEditor().addCancelListener((event) -> {
            Notification.show("Anulowano!",Notification.Type.WARNING_MESSAGE);
        });
        grid.getEditor().addSaveListener((event) -> {
            Osoba o = event.getBean();
            personList.add(o);
            Notification.show("Zapisano",Notification.Type.HUMANIZED_MESSAGE);
            grid.setItems(personList.update());
         });
        */
        grid.setItems(personList.update());
        layout.addComponent(grid);
        configGrid();
    }
    
    private void addTextField(){
        textLayout = new HorizontalLayout();
        firstName = new TextField("Imie");
        lastName = new TextField("Nazwisko");
        email = new TextField("Gabinet");
        specjalizacja = new TextField("Dyżur");
        
        textLayout.addComponents(firstName,lastName,email,specjalizacja);
        layout.addComponent(textLayout);
        
    }
    
    private void clearFields(TextField ... fields){
        Arrays.asList(fields).stream().forEach((field) -> {
            field.clear();
            field.setErrorHandler(null);
            field.setComponentError(null);
        });
    }
    
    private void addSubjectComboBox() {
        miesiac = new ComboBox("Wybierz miesiac:");
        miesiac.clear();
        miesiac.setItems("Listopad","Pazdziernik");
        /*miesiac.addValueChangeListener((event) -> {
            if(event.getValue().toString().equals("----Wybierz przedmiot -----"));
            else
            {
            update((Subject)event.getValue());
            Notification.show("Wybrano przedmiot: "+event.getValue(),Notification.Type.TRAY_NOTIFICATION);
            };
        });
*/
        miesiac.setTextInputAllowed(false);
        miesiac.setEmptySelectionCaption("Listopad");
        miesiac.setEmptySelectionAllowed(false);
        
        layout.addComponent(miesiac);
    }
}
    

