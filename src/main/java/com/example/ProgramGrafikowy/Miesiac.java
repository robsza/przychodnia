/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author robert
 */
@Entity 
public class Miesiac {
    
    private int id;
    private String name;
    private int rok;
    private int dni;
    private Set<Dyzur> dyzury;
    
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public Miesiac() {
    
    }
    @OneToMany(mappedBy = "miesiacpracy", cascade = CascadeType.ALL)
        public Set<Dyzur> getDyzury() {
            return dyzury;
        }

        public void setDyzury(Set<Dyzur> dyzury) {
            this.dyzury = dyzury;
        }

        public void setDyzury(Dyzur dyzur){
            dyzury.add(dyzur);
        }
    
    @Override
    public String toString() {
        return name;
    }
    
}
