/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author robert
 */
public interface OsobaRepository extends JpaRepository<Osoba, Long> {
    public void deleteById(int id);
    public Osoba findById (int id);
    @Transactional
    public void deleteByEmail(String email);    
    public List<Osoba> findByEmail(String email); 
}
