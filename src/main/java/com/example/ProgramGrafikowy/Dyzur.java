/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author robert
 */


@Entity 
public class Dyzur {
  
    private int id;
    
    private String name;
    
    private int dzien;
    
    private int godziny;
    
    private Osoba osobadyzurujaca;
    
    private Miesiac miesiacpracy;
    
    private Gabinet gabinetdyzuru;
    
    public Dyzur() {
        
    }

    public Dyzur(String name) {
        this.name = name;
    }

    public Dyzur(String name, Osoba osobadyzurujaca) {
        this.name = name;
        this.osobadyzurujaca = osobadyzurujaca;
    }

    public Dyzur(String name, Gabinet gabinetdyzuru) {
        this.name = name;
        this.gabinetdyzuru = gabinetdyzuru;
    }

    public Dyzur(String name, int godziny,int dzien, Osoba osobadyzurujaca, Miesiac miesiacpracy, Gabinet gabinetdyzuru) {
        this.name = name;
        this.godziny = godziny;
        this.dzien = dzien;
        this.osobadyzurujaca = osobadyzurujaca;
        this.miesiacpracy = miesiacpracy;
        this.gabinetdyzuru = gabinetdyzuru;
    }
    
    
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
        public void setName(String name) {
        this.name = name;
    }

    public int getGodziny() {
        return godziny;
    }

    public void setGodziny(int godziny) {
        this.godziny = godziny;
    }

    public int getDzien() {
        return dzien;
    }

    public void setDzien(int dzien) {
        this.dzien = dzien;
    }
    
    
    
    
    
    @ManyToOne
    @JoinColumn(name = "osoba_id")
    public Osoba getOsobadyzurujaca() {
        return osobadyzurujaca;
    }

    public void setOsobadyzurujaca(Osoba osoba) {
        this.osobadyzurujaca = osoba;
    }
    
    @ManyToOne
    @JoinColumn(name = "miesiac_id")
    public Miesiac getMiesiacpracy() {
        return miesiacpracy;
    }

    public void setMiesiacpracy(Miesiac miesiacpracy) {
        this.miesiacpracy = miesiacpracy;
    }
    
    @ManyToOne
    @JoinColumn(name = "gabinet_id")
    public Gabinet getGabinetdyzuru() {
        return gabinetdyzuru;
    }
    
    public void setGabinetdyzuru(Gabinet gabinetdyzuru) {
        this.gabinetdyzuru = gabinetdyzuru;
    }  

}
