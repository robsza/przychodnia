/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProgramGrafikowy;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author robert
 */
@SpringComponent
public class PersonList {
    @Autowired
    OsobaRepository repo;

    private List<Osoba> personList;
    
    
    public List<Osoba> update()
    {
        return repo.findAll();
    }
    public void add(Osoba person)
    {
        repo.save(person);
    }
    @Transactional
    public void delate(Osoba person){
        repo.deleteByEmail(person.getEmail());
    }
    public boolean duplicateEmail(String emial){
        List<Osoba> lista = repo.findByEmail(emial);
        if (lista.isEmpty())
            return false;
        else
            return true;
    }
}
